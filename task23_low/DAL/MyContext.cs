﻿using task23_low.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace task23_low.DAL
{
    public class MyContext : DbContext
    {
        public MyContext() : base("MyContext")
        {
        }
        public DbSet<Respond> Responds { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}