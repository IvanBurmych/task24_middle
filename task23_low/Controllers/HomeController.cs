﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using task23_low.DAL;

namespace task23_low.Controllers
{
    public class HomeController : Controller
    {
        private MyContext db = new MyContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Guest()
        {
            return View(db.Responds.ToList());
        }
        [HttpPost]
        public ActionResult gGuest(string name, string text)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(text) || ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                db.Responds.Add(new Models.Respond { Name = name, Text = text, Date = DateTime.Now }) ;
                db.SaveChanges();
            }
            return RedirectToAction("Guest");
        }
        public ActionResult Questions()
        {
            ViewBag.Message = "Your Questions page.";

            return View();
        }
        [HttpGet]
        public ActionResult qQuestions(string color)
        {
            string authData = $"color: {color}";
            return Content(authData);
        }
        [HttpPost]
        public ActionResult qQuestions(string[] colors)
        {
            string authData = "colors: ";
            foreach (string color in colors)
            {
                authData += $"{color}, ";
            }
            return Content(authData);
        }
    }
}